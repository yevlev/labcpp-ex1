/**
* A file that consists of maxIndependantSet() function (and some helpers),
* which calculates the maximum weight of an independent set, which is
* a sub-set of given collection of hash maps.
*
*/

#include <iostream> 
#include <string>

#include "MyHashMap.h"

#define NUM_ARRAYS 4

/**
* returns true iff value contained in arr.
*/
bool isInArray(int* arr, int lastFilledIndex, int value)
{
	for (int i = 0; i <= lastFilledIndex; i++)
	{
		if (arr[i] == value)
		{
			return true;
		}
	}

	return false;
}


/**
* returns an index of a hash map, from 0 to hashMapsArrSize-1, which isn't contained
* in illegalSetsIndexes array (indexws of sets that we can't use anymore),
* or -1 if there is no such index (all illegal).
*/
int getLegalIndex(int hashMapsArrSize, int illegalSetsIndexes[], int illegalSetsLastUsedIndex)
{
	for (int i = 0; i < hashMapsArrSize; i++)
	{
		if (!isInArray(illegalSetsIndexes, illegalSetsLastUsedIndex, i))
		{
			return i;
		}
	}

	return -1;
}

/**
* adds to illegalSetsIndexes all indexes of sets, which intersect with given
* set index - checkedHashMapIndex (only if they are not in illegalSetsIndexes already).
* returns the number of sets that were added.
*/
int addIntersectingSets(MyHashMap mapsArr[], int arrSize, int illegalSetsIndexes[], 
				int illegalSetsLastUsedIndex, int checkedHashMapIndex, bool** intersectionMatrix)
{
	int addedSets = 0;

	for (int i = 0; i < arrSize; i++)
	{
		/* is not already in illegalSetsIndexes. */
		if (!isInArray(illegalSetsIndexes, illegalSetsLastUsedIndex, i)) 
		{
			if (intersectionMatrix[checkedHashMapIndex][i] == true) /* they intersect. */
			{
				illegalSetsIndexes[illegalSetsLastUsedIndex + 1] = i;
				illegalSetsLastUsedIndex++;
				addedSets++;
			}
		}
	}

	return addedSets;
}


/**
* recursive helper method that calculates the maximum weight of an independent set, which is
* a sub-set of given collection of hash maps.
* based on the recursive formula that each set is either a part of the best solution, or not.
*/
double recursiveMaxSetCover(MyHashMap mapsArr[], int arrSize, 
				int illegalSetsIndexes[], int illegalSetsLastUsedIndex, bool** intersectionMatrix)
{
	double max1, max2;

	/* getting an index of set that we can add to the solution. */
	int i = getLegalIndex(arrSize, illegalSetsIndexes, illegalSetsLastUsedIndex);
	int addedSets;

	if (i == -1) /* no more legal sets to use - return max = 0. */
	{
		return 0;
	}


	/* 1st recursive case - set at index i isn't part of the solution. */
	illegalSetsIndexes[illegalSetsLastUsedIndex + 1] = i; /* can't use it further. */
	addedSets = 1;

	max1 = recursiveMaxSetCover(mapsArr, arrSize, illegalSetsIndexes, 
								illegalSetsLastUsedIndex + addedSets, intersectionMatrix); 
	
	/* 2nd recursive case - set at index i is part of the solution. */
	illegalSetsIndexes[illegalSetsLastUsedIndex + 1] = i;
	addedSets = 1;

	/* adds to illegalSetsIndexes all indexes of sets, which intersect with out set at index i. */
	addedSets += addIntersectingSets(mapsArr, arrSize, 
							illegalSetsIndexes, illegalSetsLastUsedIndex + 1, i, intersectionMatrix);

	max2 = mapsArr[i].totWeigth() + recursiveMaxSetCover(mapsArr, arrSize, 
					illegalSetsIndexes, illegalSetsLastUsedIndex + addedSets, intersectionMatrix); 


	return ((max1 > max2) ? max1 : max2);
}

/**
* function that calculates the maximum weight of an independent set, which is
* a sub-set of given collection of hash maps, in which any two sets are disjoint.
* it returns the weight of such maximum subset.
*/
double maxIndependantSet(MyHashMap mapsArr[], int arrSize)
{

	int* illegalSetsIndexes = new int[arrSize];
	int illegalSetsLastUsedInsex = -1; /* all sets are legal to use at start. */

	/* memory allocation for intersection matrix of hash maps (true if intersect, false otherwise). */
	bool** intersectionMatrix = new bool*[arrSize];

	for (int i = 0; i < arrSize; i++)
	{
		intersectionMatrix[i] = new bool[arrSize];
	}

	/* filling of intersection matrix. */
	for (int i = 0; i < arrSize; i++)
	{
		for (int j = 0; j < arrSize; j++)
		{
			intersectionMatrix[i][j] = mapsArr[i].isIntersect(mapsArr[j]);
			intersectionMatrix[j][i] = intersectionMatrix[i][j]; /* symmetric. */
		}
	}

	/* getting the solution. */
	double maxSol = recursiveMaxSetCover(mapsArr, arrSize, 
								illegalSetsIndexes, illegalSetsLastUsedInsex, intersectionMatrix);

	delete[] illegalSetsIndexes;

	/* memory freeing for intersection matrix. */
	for (int i = 0; i < arrSize; i++)
	{
		delete[] intersectionMatrix[i];
	}
	
	delete[] intersectionMatrix;

	return maxSol;
}


/**
* the main.
*
* performs tests on maxIndependantSet() function.
*/
int main()
{
	MyHashMap setsArr[NUM_ARRAYS];

	setsArr[0].add("0", 5.);
	setsArr[0].add("00", 0.6);
	setsArr[1].add("00", 2.);
	setsArr[1].add("000", 2.);
	setsArr[2].add("0", 2.);
	setsArr[2].add("0000", 2.);
	setsArr[3].add("00000", -2.);

	if (maxIndependantSet(setsArr, NUM_ARRAYS) < 5.8)
	{
		std::cout << "ERROR: Fail test ~labcpp/www/ex1/TestSetCover.cpp" << std::endl;
		return 1;
	}

	std::cout << "Pass basic maxIndependentSet test. " << std::endl;

	return 0;
}

