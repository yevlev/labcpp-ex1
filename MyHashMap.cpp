/**
 * The implementation of the MyHashMap. 
 *
 * The hash map is a data structure that provided fast accession to
 * data stored in it, and uses linked list in its implementation.
 *
 */

#include "MyHashMap.h"


/**
* The hash function.
* Input parameter - any C++ string.
* Return value: the hash value - the sum of all the characters of the string
* (according to their ASCII value) module HASH_SIZE. The hash value of the
* empty string is 0 (since there are no characters, their sum according to
* the ASCII value is 0).
* NOTE: In a better design the function would have belong to the string class
* (or to a class that is derived from std::string). We implement it as a "stand 
* alone" function since you didn't learn inheritance yet. Keep the function
* global (it's important to the automatic tests).
*
*/
int MyHashMap::myHashFunction(const std::string &str)
{
	int i = 0;
	int sum = 0;

	while (str[i] != '\0')
	{
		sum += (int)str[i];
		i++;
	}

	return (sum % HASH_SIZE);
}

/**
 * Default Constructor. Creates an empty hash map.
 */
MyHashMap::MyHashMap() : _size(0) 
{

}
	
/**
 * Destructor. 
 */
MyHashMap::~MyHashMap()
{

}

/**
 * add a string to the HashMap. Locate the entry of the relevant linked list in
 * the HashMap using the function myHashFunction, and add the element to the end of that list.
 * If the element already exists in the HashMap, change its data to the
 * input parameter. Important: unlike our MyLinkedList, that HashMap will contain at most
 * one copy of each std::string.
 */
void MyHashMap::add(const std::string &str, double data)
{
	bool isInTable = false;
	int ind = myHashFunction(str);
	double foundData;

	isInTable = this->_hashT[ind].isInList(str, foundData); /* look for given key. */

	if (isInTable == false)  /* not in table. */
	{
		this->_hashT[ind].add(str, data);
		(this->_size)++;

		return;
	}

	/* if we've got here, same key was in table - we update it with the new data. */
	this->_hashT[ind].remove(str);
	this->_hashT[ind].add(str, data);  
	/* size will be updated via add() - prev. case,  and remove(). */
}

/**
 * remove a string from the HashMap. Locate the entry of the relevant linked list in
 * the HashMap using the function myHashFunction, and remove the element from it.
 * Return one on success, or zero if the element wasn't in the HashMap.
 */
size_t MyHashMap::remove(const std::string &str)
{
	int ind = myHashFunction(str);
	bool found;

	found = this->_hashT[ind].remove(str); /* can be only in ind bucket. */

	if (found == false)
	{
		return 0;
	}

	(this->_size)--; /* was found and removed. */

	return 1;
}

/**
 * Return true if the element is in the HashMap, or false otherwise.
 * If the element exists in the hash map, return in 'data' its appropriate data
 * value. Otherwise don't change the value of 'data'.
 */
bool MyHashMap::isInHashMap(const std::string &str, double &data) const
{
	int ind = myHashFunction(str);

	return (this->_hashT[ind].isInList(str, data));
}


/**
 * Return number of elements stored in the HashMap
 */
int MyHashMap::size() const
{
	return (this->_size);
}


/**
 * Return true if and only if there exists a string that belong both to the
 * HashMap h1 and to this HashMap
 */
bool MyHashMap::isIntersect(const MyHashMap &h1) const
{
	bool intersect = false;

	for (int i = 0; i < HASH_SIZE; i++)
	{
		/* enough to check if backets at same index intersect. */
		intersect = this->_hashT[i].isListsIntersect(h1._hashT[i]);	

		if (intersect == true)
		{
			return true;
		}
	}

	return false;

}


/**
 * Return the total wight of the hash elements
 */
double MyHashMap::totWeigth() const
{
	int i = 0;
	double sum = 0;

	while (i < HASH_SIZE) /* iterates over all buckets. */
	{
		sum += this->_hashT[i].sumList();
		i++;
	}

	return sum;
}
