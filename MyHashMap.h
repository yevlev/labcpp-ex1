/**
 * MyHashMap.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a Hashmap, a data structure that provided fast accession to
 *          data stored in.
 *          Depend on MyLinkedList
 *
 *  Methods: MyHashMap() - Constructor
 *  ~MyHashMap()         - Destructor
 *
 *  add                  - Add a string to the HashMap. Locate the entry of the relevant linked list in
 *                         the HashMap using the function myHashFunction, and add the element to
 *                         the end of that list.
 *                         If the element already exists in the HashMap, change its data to the
 *                         input parameter. Important: unlike our MyLinkedList, this HashMap will
 *                         contain at most one copy of each std::string.
 *
 *  remove               - Remove a string from the HashMap. Locate the entry of the relevant linked
 *                         list in the HashMap using the function myHashFunction, and remove the element
 *                         from it.
 *                         Return true on success, or false if the element wasn't in the HashMap.
 *
 *  isInHashMap            - Return true if the element is in the HashMap, or false otherwise.
 *                         If the element exists in the HashMap, return in 'data' its appropriate data
 *                         value. Otherwise don't change the value of 'data'.
 *
 *  size                 - Return number of elements stored in the HashMap.
 *
 *  isIntersect            - Return true if and only if there exists a string that belongs both to the
 *                         HashMap h1 and to this HashMap
 *
 *  totWeigth            - Return the total weight of the hash elements
 *
 * --------------------------------------------------------------------------------------
 */


#ifndef MY_HASH_MAP_H
#define MY_HASH_MAP_H

#include <string>
#include "MyLinkedList.h"

#define HASH_SIZE 29	 /* The number of entrances of the hash table */
						 /* should be a prime number */

/**
 * The definition of the HashMap
 */
class MyHashMap
{
public:

	static int myHashFunction(const std::string &str);

	MyHashMap();
	~MyHashMap();

	void add(const std::string &str, double data);
	
	size_t remove(const std::string &str);
	
	bool isInHashMap(const std::string &str, double &data) const;
	
	int size() const;
	
	bool isIntersect(const MyHashMap &h1) const;
	
	double totWeigth() const;

private:

	MyHashMap(const MyHashMap&);
	MyHashMap& operator=(const MyHashMap&);

private:

	MyLinkedList _hashT[HASH_SIZE];
	unsigned int _size;

};

#endif
