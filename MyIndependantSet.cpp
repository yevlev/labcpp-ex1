#include "MyHashMap.h"
#include <string>
using namespace std;

/*finds the first index that can be inserted to the sub-set*/

int firstLegalIndex(int arr[], int arrSize)
{
	for (int i = 0; i < arrSize; i++)
	{
		if (arr[i] != -1)
		{
			return arr[i];
		}
	}

	return -1;
}

void markIntersecting(MyHashMap mapsArr[], int leftIndexes[], int arrSize, int ind)
{
	for (int i = 0; i < arrSize; i++)
	{
		if (leftIndexes[i] != -1)
		{
			if (mapsArr[ind].isIntersect(mapsArr[i]))
			{
				leftIndexes[i] = -1;
			}
		}
	}
}

void copyArrays(int* destination, int* source, int arrSize)
{
	for (int j = 0; j < arrSize; j++)
	{
		destination[j] = source[j];
	}
}

double recMaxSetCover(MyHashMap mapsArr[], int leftIndexes[], int arrSize)
{
	double max1, max2;
	int i = firstLegalIndex(leftIndexes, arrSize);

	if (i == -1)
	{
		return 0;
	}

	leftIndexes[i] = -1; 

	int* copyLeftIndexes = new int[arrSize];
	copyArrays(copyLeftIndexes, leftIndexes, arrSize);

	max1 = recMaxSetCover(mapsArr, copyLeftIndexes, arrSize); /* in case the subset doesn't contain the current set */
	delete[] copyLeftIndexes;

	markIntersecting(mapsArr, leftIndexes, arrSize, i); /* marking the intersecting hash-maps that can't be in the set */
	max2 = mapsArr[i].totWeigth() + recMaxSetCover(mapsArr, leftIndexes, arrSize); /* in case the subset contains the current set */
	

	return ((max1 > max2) ? max1 : max2);

}

double maxIndependantSet(MyHashMap mapsArr[], int arrSize)
{

	int* leftIndexes = new int[arrSize];
	
	for (int i = 0; i < arrSize; i++)
	{
		leftIndexes[i] = i;
	}

	double maxSol = recMaxSetCover(mapsArr, leftIndexes, arrSize);

	delete[] leftIndexes;

	return maxSol;
}





