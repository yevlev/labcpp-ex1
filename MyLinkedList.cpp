/**
 * The implementation of the MyLinkedList class. 
 *
 * The linked list is a data structure that provides fast insertion of data,
 * and also removal and search of data.
 *
 */

#include <iostream>

#include "MyLinkedList.h"


/**
* Default Constructor. Creates an empty linked list.
*/
MyLinkedList::MyLinkedList() : _head(NULL) , _tail(NULL)
{

}

/**
* Destructor.
*/
MyLinkedList::~MyLinkedList()
{
	MyLinkedListNode* curr = this->_head;

	while (curr != NULL)  /* freeing memory of all list nodes. */
	{
		MyLinkedListNode* temp = curr->getNext();

		delete curr;
		curr = temp;  /* curr gets next node. */
	}
}

/**
* getter for list head.
*/
MyLinkedListNode* MyLinkedList::getHead() const
{
	return this->_head;
}

/**
* getter for list tail.
*/
MyLinkedListNode* MyLinkedList::getTail() const
{
	return this->_tail;
}

/**
* adds new node with given key and data at end of this list (in O(1)).
*/
void MyLinkedList::add(const string& key, double data)
{

	MyLinkedListNode* toAdd = new MyLinkedListNode(key, data, NULL, NULL);

	if (this->_head == NULL) /* list is empty. */
	{
		this->_head = toAdd;
		this->_tail = toAdd;

		return;  /* we are done. */
	}
	
	/* if we got here, list wasn't empty - we add at the end. */

	toAdd->setPrev(this->_tail);

	this->_tail->setNext(toAdd);
	this->_tail = toAdd;

}

/**
* helper method for remove().
* removes given node from this linked list.
*/
void MyLinkedList::removeLink(const MyLinkedListNode* node)
{

	if (node->getPrev() != NULL)  /* node isn't the head. */
	{
		node->getPrev()->setNext(node->getNext());
	}
	else  /* node is the head. */
	{
		this->_head = node->getNext();
	}

	if (node->getNext() != NULL)   /* node isn't the tail. */
	{
		node->getNext()->setPrev(node->getPrev());
	}
	else   /* node is the tail. */
	{
		this->_tail = node->getPrev();
	}
}


/**
* removes all nodes that match given key. 
* returns true iff any were removed.
*/
bool MyLinkedList::remove(const string& key)
{
	MyLinkedListNode* curr = this->getHead();
	MyLinkedListNode* temp;
	bool wasDeleted = false;

	while (curr != NULL)
	{
		if (curr->getKey() == key) /* key matches. */
		{
			temp = curr->getNext(); 
			removeLink(curr);  /* curr is unlinked from the list.*/
			
			delete curr;
			wasDeleted = true;

			curr = temp;
		}
		else
		{
			curr = curr->getNext();
		}

	}

	return wasDeleted;
}
	       
	
/**
* returns true iff given key appears in list at least once.
* returns, using the double ref. parameter, the value of the 1st found (if any), starting at head.
*/
bool MyLinkedList::isInList(const string& key, double& data) const
{
	MyLinkedListNode* curr = this->_head;

	while (curr != NULL) 
	{
		if (curr->getKey() == key) /* found. */
		{	
			data = curr->getData();
			return true;
		}

		curr = curr->getNext();
	}

	return false;  /* not found. */
}

/**
* returns sum of data numbers in the list.
*/
double MyLinkedList::sumList() const
{
	double sum = 0;
	MyLinkedListNode* curr = this->_head;

	while (curr != NULL) 
	{
		sum += curr->getData();
		curr = curr->getNext();
	}

	return sum;
}
	
/**
* prints all list entries, each in new line.
*/
void MyLinkedList::printList() const
{
	MyLinkedListNode* curr = this->_head;

	if (curr == NULL)  /* list is empty. */
	{
		cout << "Empty" << endl;
		return;
	}

	while (curr != NULL)
	{
		cout << curr->getKey() << "," << curr->getData() << endl;
	
		curr = curr->getNext();
	}
}


/**
* returns true iff set of keys of this list intersects 
* with set of keys of other, given list.
*/
bool MyLinkedList::isListsIntersect(const MyLinkedList& other) const
{
	MyLinkedListNode* node = this->getHead();
	double foundData; 

	while (node != NULL)
	{
		if (other.isInList(node->getKey(), foundData) == true)  /* common key. */
		{
			return true;
		}

		node = node->getNext();
	}

	return false;  /* disjoint. */
}

