/**
 * MyLinkedList.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a doubly linked list, 
 *			a data structure that provides fast insertion of data.
 *
 *  Methods: 
 *
 *  MyLinkedList()		 - Default Constructor. Creates an empty linked list.
 *  ~MyLinkedList()		 - Destructor.
 *
 *
 * add					 - adds new node with given key and data at end of the list (in O(1)).
 *
 * remove				 - removes all nodes that match given key. 
 *						   returns true iff any were removed.
 *
 * isInList				 - returns true iff given key appears in list at least once.
 *						   returns, using the double ref. parameter, the value of 1st found.
 *
 * sumList				 - return sum of numbers in the list.
 *
 * printList			 - prints all list entries, each in new line.
 * 
 * isListsIntersect      - returns true iff set of keys of this list intersects 
 *					       with set of keys of other, given list.
 *
 * --------------------------------------------------------------------------------------
 */

#ifndef MY_LINKED_LIST_H
#define MY_LINKED_LIST_H

#include <string>

#include "MyLinkedListNode.h"

using namespace std;

/**
 * The definition of the LinkedList.
 */
class MyLinkedList
{

public:

	MyLinkedList();
	~MyLinkedList();

	void add(const string& key, double data);
	bool remove(const string& key);
	bool isInList(const string& key, double& data) const;

	double sumList() const;
	void printList() const;

	bool isListsIntersect(const MyLinkedList& other) const;

private:
	
	MyLinkedListNode* getHead() const;
	MyLinkedListNode* getTail() const;

	void removeLink(const MyLinkedListNode* link);

	MyLinkedListNode* _head;
	MyLinkedListNode* _tail;

};

#endif
