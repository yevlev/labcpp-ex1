/**
 * The implementation of the MyLinkedListNode class.  
 *
 * The node is a unit in a linked list, and holds string key, double data,
 * and pointers to next and prev. nodes.
 */

#include "MyLinkedListNode.h"

/**
* Constructor. Constructs a node, with given key, data, next and prev. nodes.
*/
MyLinkedListNode::MyLinkedListNode(const string& key, double data, 
											MyLinkedListNode* next, MyLinkedListNode* prev) 
											: _key(key), _data(data), _next(next), _prev(prev)
{

}

/**
* Destructor.
*/
MyLinkedListNode::~MyLinkedListNode()
{

}

/**
* getter for key.
*/
const string& MyLinkedListNode::getKey() const
{
	return this->_key;
}

/**
* getter for data.
*/
double MyLinkedListNode::getData() const
{
	return this->_data;
}

/**
* getter for next node.
*/
MyLinkedListNode* MyLinkedListNode::getNext() const
{
	return this->_next;
}

/**
* getter for prev. node.
*/
MyLinkedListNode* MyLinkedListNode::getPrev() const
{
	return this->_prev;
}

/**
* setter for next node.
*/
void MyLinkedListNode::setNext(MyLinkedListNode* node)
{
	this->_next = node;
}

/**
* setter for prev. node.
*/
void MyLinkedListNode::setPrev(MyLinkedListNode* node)
{
	this->_prev = node;
}

