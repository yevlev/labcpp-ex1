/**
 * MyLinkedListNode.h 
 *
 * --------------------------------------------------------------------------------------
 * General: This class represents a single node in a linked list, where each
 *          node holds a string key and a double value, and pointers to next and prev. nodes.
 *
 *  Methods: 
 *
 *  MyLinkedListNode()   - Constructor. Constructs a node, with given key, data, next and prev. nodes.
 *  ~MyLinkedListNode()  - Destructor.
 *
 *  getKey()             - Getter for node's key.
 *
 *  getData()            - Getter for node's data.
 *
 *  getNext()            - Getter for next node.
 *
 *  getNext()            - Getter for prev. node.
 *
 *  setNext()            - Setter for next node.
 *
 *  setPrev()            - Setter for prev. node.
 *
 * --------------------------------------------------------------------------------------
 */
#ifndef MY_LINKED_LIST_NODE_H
#define MY_LINKED_LIST_NODE_H

#include <string>

using namespace std;

/**
 * The definition of the LinkedListNode
 */
class MyLinkedListNode
{

public:

	MyLinkedListNode(const string& key, double data, MyLinkedListNode* next, MyLinkedListNode* prev);
	~MyLinkedListNode();

	const string& getKey() const;
	double getData() const;
	MyLinkedListNode* getNext() const;
	MyLinkedListNode* getPrev() const;

	void setNext(MyLinkedListNode* link);
	void setPrev(MyLinkedListNode* link);

private:

	const string _key;
	const double _data;
	MyLinkedListNode* _next;
	MyLinkedListNode* _prev;

};

#endif
